const ntwConverter = require("number-to-words");

function convertTimeToWords(hour, minute) {
  _validation(hour, minute);
  const TIME_DIRECTION = {
    PAST: 0,
    TO: 1,
    ZERO: 3,
  };
  let mHour = 0;
  let mMinute = 0;
  let timeDirection;

  if (minute > 0 && minute <= 30) {
    mHour = hour;
    mMinute = minute;
    timeDirection = TIME_DIRECTION.PAST;
  } else if (minute > 30) {
    timeDirection = TIME_DIRECTION.TO;
    mHour = hour + 1;
    mHour = mHour == 13 ? 1 : mHour;
    mMinute = 60 - minute;
  } else {
    mHour = hour;
    mMinute = minute;
    timeDirection = TIME_DIRECTION.ZERO;
  }

  const hourInWords = _convertHourToWords(mHour);
  const minuteInWords = _convertMinuteToWords(mMinute);

  let timeInWords;
  if (timeDirection == TIME_DIRECTION.PAST) {
    timeInWords = `${minuteInWords} past ${hourInWords}`;
  } else if (timeDirection == TIME_DIRECTION.TO) {
    timeInWords = `${minuteInWords} to ${hourInWords}`;
  } else if (timeDirection == TIME_DIRECTION.ZERO) {
    timeInWords = `${hourInWords} o'clock`;
  }

  return timeInWords;
}

function _validation(hour, minute) {
  if (!_checkIsInteger(hour)) {
    throw new TypeError("The hour argument must be of integer type");
  }

  if (!_checkIsInteger(minute)) {
    throw new TypeError("The minute argument must be of integer type");
  }

  if (hour < 1 || hour > 12) {
    throw new RangeError("The hour argument must be 0<hour<13");
  }

  if (minute < 0 || minute > 59) {
    throw new RangeError("The minute argument must be -1<minute<60");
  }
}

function _checkIsInteger(target) {
  return target === parseInt(target, 10);
}

function _convertNumberToWords(number) {
  const words = ntwConverter.toWords(number).replace("-", " ");
  return words;
}

function _convertHourToWords(hour) {
  const hourInWords = _convertNumberToWords(hour);
  return hourInWords;
}
function _convertMinuteToWords(minute) {
  let minuteInWords = "";
  if (minute == 15) {
    minuteInWords = "quarter";
  } else if (minute == 30) {
    minuteInWords = "half";
  } else {
    const minuteText = minute > 1 ? "minutes" : "minute";
    minuteInWords = `${_convertNumberToWords(minute)} ${minuteText}`;
  }
  return minuteInWords;
}
